json.array!(@patients) do |patient|
  json.extract! patient, :id, :first_name, :last_name, :date_of_birth, :address, :phone, :email
  json.url patient_url(patient, format: :json)
end
