json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :patient_id, :appointment_schedule_id, :diagnostic_code_id
  json.url invoice_url(invoice, format: :json)
end

