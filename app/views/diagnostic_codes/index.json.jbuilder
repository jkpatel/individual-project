json.array!(@diagnostic_codes) do |diagnostic_code|
  json.extract! diagnostic_code, :id, :name, :price, :created_at, :updated_at, :code
  json.url diagnostic_code_url(diagnostic_code, format: :json)
end
