json.array!(@appointment_schedules) do |appointment_schedule|
  json.extract! appointment_schedule, :id, :patient_id, :physician_id, :diagnostic_code_id, :reason, :date_rdv, :hour_rdv, :price, :created_at, :updated_at
  json.url appointment_schedule_url(appointment_schedule, format: :json)
end
