class Physician < ActiveRecord::Base
  validates_presence_of :name
  has_many :patients
  has_many :appointment_schedules
  has_many :notes
  def to_s
    "#{name}"
  end
end
