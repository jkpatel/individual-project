class Invoice < ActiveRecord::Base
  validates_presence_of :patient_id
  validates_presence_of :diagnostic_code_id
  validates_presence_of :appointment_schedule_id
  validates_presence_of :price, :greater_than => 0

  belongs_to :patient
  belongs_to :diagnostic_code
  belongs_to :appointment_schedule
end
