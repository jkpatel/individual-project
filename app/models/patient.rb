class Patient < ActiveRecord::Base
  validates_presence_of :first_name
  validates_presence_of :last_name

  has_many :appointment_schedules

  has_many :invoice
  belongs_to :physician

  def name
    "#{first_name} #{last_name}"
  end
  def to_s
    name
  end

end
