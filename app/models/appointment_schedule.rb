class AppointmentSchedule < ActiveRecord::Base
  validates_presence_of :patient_id
  validates_presence_of :physician_id

  validates_presence_of :date_rdv
  validates_presence_of :hour_rdv

  validate :valid_date?



  has_many :notes
  belongs_to :diagnostic_code
  belongs_to :patient
  belongs_to :physician
  has_many :invoices

  Time_Work= [
      "08:00 AM", "08:30 AM", "09:00 AM", "09:30 AM",
      "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM",
      "12:00 PM", "12:30 PM", "01:00 PM", "01:30 PM",
      "02:00 PM", "02:30 PM", "03:00 PM", "03:30 PM",
      "04:00 PM" ]

  def valid_date?

    if Date.today > self.date_rdv
      errors.add :base, "Invalid date: date is prior than today."
    end
    day = self.date_rdv.strftime("%A")
    if ["Saturday", "Sunday"].include?(day.capitalize)
      errors.add :base, "Invalid date: cannot make an appointment on the weekend."
    end
  end

  def time_appointment
    "The #{date_rdv} at #{hour_rdv}"
  end

  def to_s
    "#{patient}: #{time_appointment} "
  end
end
