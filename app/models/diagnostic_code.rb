class DiagnosticCode < ActiveRecord::Base
  has_many :appointment_schedules
  has_many :invoices

  validates_presence_of :name
  validates_presence_of :price, :greater_than => 0
  validates_uniqueness_of :code

  def to_s
    "#{code} #{name}"
  end
end
