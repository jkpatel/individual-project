module ApplicationHelper
  def convert_to_date(date)
    return date unless date.to_s.include?('/')
    dates_part = date.split('/')
    date = "#{dates_part[2]}-#{dates_part[0]}-#{dates_part[1]}" if dates_part.size==3
    date
  end
end
