class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]

  # GET /invoices
  # GET /invoices.json
  def index
    @invoices = Invoice.order("created_at DESC").paginate(:page=>params[:page], :per_page=>15)
  end

  # GET /invoices/1
  # GET /invoices/1.json
  def show
  end

  # GET /invoices/new
  def new
    @invoice = Invoice.new(patient_id:params[:patient_id], appointment_schedule_id:params[:appointment_schedule_id])
    @app_schedule = AppointmentSchedule.where("diagnostic_code_id is not null")
    invoices = Invoice.where(appointment_schedule_id: @app_schedule.map(&:id)).map(&:appointment_schedule_id)
    app = @app_schedule.reject{|ap| invoices.include? ap.id }
    @patients= Patient.where(id: app.map(&:patient_id))
    respond_to do |format|
      format.html {  }
      format.js {
        if params[:appointment_schedule_id]
          @invoice.diagnostic_code_id = AppointmentSchedule.find(params[:appointment_schedule_id]).diagnostic_code_id rescue nil
          @invoice.price = DiagnosticCode.find(@invoice.diagnostic_code_id).price rescue 0
        end
        @app_schedule = AppointmentSchedule.where("diagnostic_code_id is not null").where(patient_id: params[:patient_id])
        invoices = Invoice.where(appointment_schedule_id: @app_schedule.map(&:id)).map(&:appointment_schedule_id)
        @app_schedule = @app_schedule.reject{|ap| invoices.include? ap.id }
      }

    end
  end

  # GET /invoices/1/edit
  def edit
  end

  # POST /invoices
  # POST /invoices.json
  def create
    @invoice = Invoice.new(invoice_params)
    if params[:invoice][:appointment_schedule_id]
      @invoice.diagnostic_code_id = AppointmentSchedule.find(params[:invoice][:appointment_schedule_id]).diagnostic_code_id rescue nil
    end

    respond_to do |format|
      if @invoice.save
        format.html { redirect_to @invoice, notice: 'Invoice was successfully created.' }
        format.json { render :show, status: :created, location: @invoice }
      else
        format.html {
          flash[:error] = @invoice.errors.full_messages.join(', ')
          redirect_to new_invoice_path }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoices/1
  # PATCH/PUT /invoices/1.json
  def update
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to @invoice, notice: 'Invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @invoice }
      else
        format.html { render :edit }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invoices/1
  # DELETE /invoices/1.json
  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to invoices_url, notice: 'Invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_invoice
    @invoice = Invoice.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def invoice_params
    params.require(:invoice).permit(:patient_id, :appointment_schedule_id, :diagnostic_code_id, :price)
  end
end
