class AppointmentSchedulesController < ApplicationController
  include ApplicationHelper
  before_action :set_rdv, only: [:show, :edit, :update, :destroy]

  before_action :set_date, only: [:create, :update]
  # GET /rdvs
  # GET /rdvs.json
  def index
    patient_id = params[:patient_id]
    if patient_id
      @appointment_schedules = AppointmentSchedule.where(patient_id: patient_id).order("date_rdv DESC ,hour_rdv DESC").paginate(:page=>params[:page], :per_page=>15)
    else
      physician_id = params[:physician_id]
      if physician_id
        @appointment_schedules = AppointmentSchedule.where(physician_id: physician_id).order("date_rdv DESC, hour_rdv DESC").paginate(:page=>params[:page], :per_page=>15)
      else
        @appointment_schedules = AppointmentSchedule.all.order("date_rdv DESC, hour_rdv DESC").paginate(:page=>params[:page], :per_page=>15)
      end
    end

  end

  def get_time_available
    date = params[:date_rdv]
    phys = params[:physician_id]
    return if date.blank?
    if phys and phys.present?
      rdvs = AppointmentSchedule.where(date_rdv: date).where(physician_id: phys).map(&:hour_rdv)
    else
      rdvs = AppointmentSchedule.where(date_rdv: date).map(&:hour_rdv)
    end

    @time = AppointmentSchedule::Time_Work
    @times= @time.select{|time| !rdvs.include?(time)}
  end

  # GET /rdvs/1
  # GET /rdvs/1.json
  def show
  end

  # GET /rdvs/new
  def new
    patient = params[:patient_id]
    @appointment_schedule = AppointmentSchedule.new(patient_id: patient)
  end

  # GET /rdvs/1/edit
  def edit
    rdvs = AppointmentSchedule.where(date_rdv: @appointment_schedule.date_rdv).where("id != ?", @appointment_schedule.id).map(&:hour_rdv)
    @times= AppointmentSchedule::Time_Work
    @times.reject!{|time| rdvs.include?(time)}
  end

  # POST /rdvs
  # POST /rdvs.json
  def create
    @appointment_schedule = AppointmentSchedule.new(rdv_params)

    respond_to do |format|
      if @appointment_schedule.save
        format.html { redirect_to patient_appointment_schedule_path(@appointment_schedule.patient_id, @appointment_schedule), notice: 'Appointment was successfully created.' }
        format.json { render :show, status: :created, location: @appointment_schedule }
      else
        format.html { render :new }
        format.json { render json: @appointment_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rdvs/1
  # PATCH/PUT /rdvs/1.json
  def update

    params[:appointment_schedule][:hour_rdv] = @appointment_schedule.hour_rdv if params[:appointment_schedule][:hour_rdv].nil? or params[:appointment_schedule][:hour_rdv].blank?
    respond_to do |format|
      if @appointment_schedule.update(rdv_params)
        format.html { redirect_to patient_appointment_schedule_path(@appointment_schedule.patient_id, @appointment_schedule), notice: 'Appointment was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment_schedule }
      else
        format.html { rdvs = AppointmentSchedule.where(date_rdv: @appointment_schedule.date_rdv).where("id != ?", @appointment_schedule.id).map(&:hour_rdv)
        @times= AppointmentSchedule::Time_Work
        @times.reject!{|time| rdvs.include?(time)}
        render :edit }
        format.json { render json: @appointment_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rdvs/1
  # DELETE /rdvs/1.json
  def destroy
    if Date.today != @appointment_schedule.date_rdv or (@appointment_schedule.hour_rdv.to_datetime - Time.now)>8.hours
      @appointment_schedule.destroy
      respond_to do |format|
        format.html { redirect_to patient_path(params[:patient_id]), notice: 'Appointment was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = "Cannot cancel this appointment because it's for #{@appointment_schedule.hour_rdv - Time.now}"
          redirect_to :back
        }
        format.json { head :no_content }
      end
    end

  end
  private
  # Use callbacks to share common setup or constraints between actions.
  def set_rdv
    @appointment_schedule = AppointmentSchedule.find(params[:id])
  end

  def set_date
    date = params[:appointment_schedule][:date_rdv]
    params[:appointment_schedule][:date_rdv] = convert_to_date(date)
    params[:appointment_schedule][:hour_rdv] = params[:appointment_schedule_hour_rdv] if params[:appointment_schedule][:hour_rdv].blank?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def rdv_params
    params.require(:appointment_schedule).permit(:patient_id,:physician_id, :date_rdv, :hour_rdv, :reason, :diagnostic_code_id)
  end
end
