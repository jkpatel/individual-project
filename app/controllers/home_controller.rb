class HomeController < ApplicationController
  include ApplicationHelper
  def index
  end

  def report
    respond_to do |format|
      format.html{ @physicians = Physician.all.includes(:appointment_schedules) }
      format.js{
        params[:end_date] = Date.today if params[:end_date].blank?
        @physicians = Physician.where(id: 1).includes(:appointment_schedules) .joins(:appointment_schedules).where("appointment_schedules.date_rdv between ? and ?",
                                                                                                                   convert_to_date(params[:begin_date]) ,convert_to_date(params[:end_date]))

      }
    end
  end

  def invoice
    @rdvs = AppointmentSchedule.where("diagnostic_code_id is null")
  end




end

