class CreateAppointmentSchedules < ActiveRecord::Migration
  def change
    create_table :appointment_schedules do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.integer :diagnostic_code_id
      t.text :reason
      t.date :date_rdv
      t.string :hour_rdv
      t.float :price
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
