class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :first_name
      t.string :last_name
      t.date :date_of_birth
      t.text :address
      t.string :phone
      t.string :email
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
