class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :appointment_schedule_id
      t.text :note
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
