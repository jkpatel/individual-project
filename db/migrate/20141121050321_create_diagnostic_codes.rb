class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.string :name
      t.float :price
      t.datetime :created_at
      t.datetime :updated_at
      t.string :code

      t.timestamps
    end
  end
end
