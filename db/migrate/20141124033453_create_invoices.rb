class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :patient_id
      t.integer :appointment_schedule_id
      t.integer :diagnostic_code_id
      t.float :price
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
