Rails.application.routes.draw do


  resources :invoices

  resources :notes,except:[:edit, :new]

  resources :patients do
    resources :appointment_schedules
  end

  resources :appointment_schedules do
    resources :notes
  end

  resources :diagnostic_codes do
    member do
      get :get_price
    end
  end

  resources :physicians do
    resources :appointment_schedules, only:[:index,:show,:edit]
  end

  get 'home/index'
  root :to=> "home#index"

  get "/admin" ,to: "home#admin"
  get "/physician_page",to: "home#physician"
  get "/office_worker",to: "home#office_worker"
  get "/patient_page",to: "home#patient"

  get "/report" ,to: "home#report"
  post "/search" ,to: "home#report"
  get "/get_time_available", to: "appointment_schedules#get_time_available"

  get "/invoice" , to: "home#invoice"


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
