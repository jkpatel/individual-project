require 'test_helper'

class AppointmentSchedulesControllerTest < ActionController::TestCase
  setup do
    @appointment_schedule = appointment_schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:appointment_schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create appointment_schedule" do
    assert_difference('AppointmentSchedule.count') do
      post :create, appointment_schedule: { created_at: @appointment_schedule.created_at, date_rdv: @appointment_schedule.date_rdv, diagnostic_code_id: @appointment_schedule.diagnostic_code_id, hour_rdv: @appointment_schedule.hour_rdv, patient_id: @appointment_schedule.patient_id, physician_id: @appointment_schedule.physician_id, price: @appointment_schedule.price, reason: @appointment_schedule.reason, updated_at: @appointment_schedule.updated_at }
    end

    assert_redirected_to appointment_schedule_path(assigns(:appointment_schedule))
  end

  test "should show appointment_schedule" do
    get :show, id: @appointment_schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @appointment_schedule
    assert_response :success
  end

  test "should update appointment_schedule" do
    patch :update, id: @appointment_schedule, appointment_schedule: { created_at: @appointment_schedule.created_at, date_rdv: @appointment_schedule.date_rdv, diagnostic_code_id: @appointment_schedule.diagnostic_code_id, hour_rdv: @appointment_schedule.hour_rdv, patient_id: @appointment_schedule.patient_id, physician_id: @appointment_schedule.physician_id, price: @appointment_schedule.price, reason: @appointment_schedule.reason, updated_at: @appointment_schedule.updated_at }
    assert_redirected_to appointment_schedule_path(assigns(:appointment_schedule))
  end

  test "should destroy appointment_schedule" do
    assert_difference('AppointmentSchedule.count', -1) do
      delete :destroy, id: @appointment_schedule
    end

    assert_redirected_to appointment_schedules_path
  end
end
